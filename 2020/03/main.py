""" AoC 2020 - Day 3: Toboggan Trajectory

Problem: https://adventofcode.com/2020/day/3
"""

from math import prod
from pathlib import Path
from typing import List, Tuple


def check_slope(data: List[str], slope: Tuple[int, int]) -> int:
    width = len(data[0])

    return sum(
        line[slope[0] * (row // slope[1]) % width] == '#'
        for row, line in enumerate(data)
        if row % slope[1] == 0
    )


def part1(data: List[str]) -> int:
    return check_slope(data, (3, 1))


def part2(data: List[str]) -> int:
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

    return prod(check_slope(data, slope) for slope in slopes)


def test() -> None:
    data = ['..##.......',
            '#...#...#..',
            '.#....#..#.',
            '..#.#...#.#',
            '.#...##..#.',
            '..#.##.....',
            '.#.#.#....#',
            '.#........#',
            '#.##...#...',
            '#...##....#',
            '.#..#...#.#']
    assert part1(data) == 7
    assert part2(data) == 336


if __name__ == '__main__':
    input_path = Path(__file__).parent / 'input'
    data = open(input_path, 'r').read().splitlines()
    print(f'Read {len(data)} lines from input file.')

    test()
    print(f'Part One: {part1(data)}')
    print(f'Part Two: {part2(data)}')
