""" AoC 2020 - Day 4: Passport Processing

Problem: https://adventofcode.com/2020/day/4
"""

import re
from pathlib import Path
from typing import List

required_fields = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}


def part1(data: List[str]) -> int:
    return sum(
        required_fields.issubset(re.findall(r'(\w+):[^\s]*(?:\s|$)', passport))
        for passport in data
    )


def part2(data: List[str]) -> int:
    validator = {
        'byr': lambda a: 1920 <= int(a) <= 2002,
        'iyr': lambda a: 2010 <= int(a) <= 2020,
        'eyr': lambda a: 2020 <= int(a) <= 2030,
        'hgt': lambda a: (a.endswith('cm') and 150 <= int(a[:-2]) <= 193) or
                         (a.endswith('in') and 59 <= int(a[:-2]) <= 76),
        'hcl': lambda a: re.match(r'^#[0-9a-f$]{6}', a),
        'ecl': lambda a: a in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'},
        'pid': lambda a: re.match(r'^\d{9}$', a),
        'cid': lambda a: True
    }

    return sum(
        all(validator[key](value) for key, value in fields.items())
        for passport in data
        if required_fields.issubset(
            fields := dict(field.split(':', 1) for field in passport.split())
        )
    )


def test() -> None:
    data = [
        'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n'
        'byr:1937 iyr:2017 cid:147 hgt:183cm',

        'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n'
        'hcl:#cfa07d byr:1929',

        'hcl:#ae17e1 iyr:2013\n'
        'eyr:2024\n'
        'ecl:brn pid:760753108 byr:1931\n'
        'hgt:179cm',

        'hcl:#cfa07d eyr:2025 pid:166559648\n'
        'iyr:2011 ecl:brn hgt:59in'
    ]
    assert part1(data) == 2
    assert part2(data) == 2


if __name__ == '__main__':
    input_path = Path(__file__).parent / 'input'
    data = open(input_path, 'r').read().split('\n\n')
    print(f'Read {len(data)} lines from input file.')

    test()
    print(f'Part One: {part1(data)}')
    print(f'Part Two: {part2(data)}')
