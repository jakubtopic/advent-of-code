""" AoC 2020 - Day 5: Binary Boarding

Problem: https://adventofcode.com/2020/day/5
"""

from pathlib import Path
from typing import List


def parse_data(data: List[str]) -> List[int]:
    return [
        int(''.join('1' if c in 'BR' else '0' for c in seat), 2)
        for seat in data
    ]


def part1(data: List[str]) -> int:
    return max(parse_data(data))


def part2(data) -> int:
    return sum(seats := parse_data(data)) - sum(range(min(seats), max(seats)))


def test() -> None:
    data = ['BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL']
    assert parse_data(data) == [567, 119, 820]
    assert part1(data) == 820


if __name__ == '__main__':
    input_path = Path(__file__).parent / 'input'
    data = open(input_path, 'r').read().splitlines()
    print(f'Read {len(data)} lines from input file.')

    test()
    print(f'Part One: {part1(data)}')
    print(f'Part Two: {part2(data)}')
