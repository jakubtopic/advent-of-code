""" AoC 2020 - Day 1: Report Repair

Problem: https://adventofcode.com/2020/day/1
"""
from pathlib import Path
from typing import Set


def part1(data: Set[int]) -> int:
    for addend1 in data:
        if (addend2 := 2020 - addend1) in data:
            return addend1 * addend2
    return 0


def part2(data: Set[int]) -> int:
    for addend1 in data:
        for addend2 in data:
            if (addend3 := 2020 - addend1 - addend2) in data:
                return addend1 * addend2 * addend3
    return 0


def test() -> None:
    data = {1721, 979, 366, 299, 675, 1456}
    assert part1(data) == 514579
    assert part2(data) == 241861950


if __name__ == '__main__':
    input_path = Path(__file__).parent / 'input'
    data = set(map(int,open(input_path, 'r').read().splitlines()))
    print(f'Read {len(data)} lines from input file.')

    test()
    print(f'Part One: {part1(data)}')
    print(f'Part Two: {part2(data)}')
