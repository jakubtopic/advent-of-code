""" AoC 2020 - Day 2: Password Philosophy

Problem: https://adventofcode.com/2020/day/2
"""

import re
from pathlib import Path
from typing import List, Tuple


def part1(data: List[Tuple[int, int, str, str]]) -> int:
    return sum(
        policy_min <= password.count(letter) <= policy_max
        for policy_min, policy_max, letter, password in data
    )


def part2(data: List[Tuple[int, int, str, str]]) -> int:
    return sum(
        (password[policy_min - 1] == letter) !=
        (password[policy_max - 1] == letter)
        for policy_min, policy_max, letter, password in data
    )


def parse_line(line: str) -> Tuple[int, int, str, str]:
    p_min, p_max, l, p = re.match(r'(\d+)-(\d+) (.): (.+)', line).groups()
    return int(p_min), int(p_max), l, p


def test() -> None:
    data = list(map(
        parse_line, ['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc']
    ))
    assert part1(data) == 2
    assert part2(data) == 1


if __name__ == '__main__':
    input_path = Path(__file__).parent / 'input'
    data = list(map(parse_line, open(input_path, 'r').read().splitlines()))
    print(f'Read {len(data)} lines from input file.')

    test()
    print(f'Part One: {part1(data)}')
    print(f'Part Two: {part2(data)}')
